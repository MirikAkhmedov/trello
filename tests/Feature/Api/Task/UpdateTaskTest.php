<?php

namespace Tests\Feature\Api\Task;

use App\Models\Desk;
use App\Models\Task;
use Tests\TestCase;

class UpdateTaskTest extends TestCase
{
    public function test_update_a_task()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();
        $task = $card->tasks()->inRandomOrder()->first();

        $changes = [
            'name' => 'updated task'
        ];

        $this->patchJson(route('desks.lists.cards.tasks.update', [
            $desk->id,
            $list->id,
            $card->id,
            $task->id
        ]), $changes)
            ->assertOk();

        $this->assertDatabaseHas('tasks', $changes);
        $this->assertTrue(Task::whereName($changes['name'])->exists());
    }
}
