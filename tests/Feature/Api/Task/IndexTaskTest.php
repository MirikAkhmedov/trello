<?php

namespace Tests\Feature\Api\Task;

use App\Models\Desk;
use Tests\TestCase;

class IndexTaskTest extends TestCase
{
    public function test_show_a_task()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $card = $list->cards()->inRandomOrder()->first();
        $task = $card->tasks()->inRandomOrder()->first();

        $this->getJson(route('desks.lists.cards.tasks.show', [
            $desk->id,
            $list->id,
            $card->id,
            $task->id
        ]))
            ->assertOk()
            ->assertJsonData([
                'id' => $task->id
            ]);
    }
}
