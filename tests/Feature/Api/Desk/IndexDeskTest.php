<?php

namespace Tests\Feature\Api\Desk;

use Tests\TestCase;

class IndexDeskTest extends TestCase
{
    public function test_show_all_desks()
    {
        $this->getJson(route('desks.index'))
            ->assertOk()
            ->assertSee([
                'id' => 1
            ]);
    }
}
