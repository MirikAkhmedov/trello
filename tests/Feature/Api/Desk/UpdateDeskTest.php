<?php

namespace Tests\Feature\Api\Desk;

use App\Models\Desk;
use Tests\TestCase;

class UpdateDeskTest extends TestCase
{
    public function test_update_a_desk()
    {
        $desk = Desk::first();

        $changes = [
            'name' => 'mirik'
        ];

        $this->putJson(route('desks.update', $desk), $changes)->assertOk();

        $this->assertDatabaseHas('desks', $changes);
        $this->assertTrue(Desk::whereName($changes['name'])->exists());
    }
}
