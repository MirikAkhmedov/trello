<?php

namespace Tests\Feature\Api\List;

use App\Models\Desk;
use Tests\TestCase;

class IndexListTest extends TestCase
{
    public function test_show_a_list()
    {
        $desk = Desk::inRandomOrder()->first();
        $list = $desk->lists()->inRandomOrder()->first();
        $this->getJson(route('desks.lists.show', [$desk->id, $list->id]))
            ->assertOk()
            ->assertJsonData([
                'id' => $list->id
            ]);
    }
}
