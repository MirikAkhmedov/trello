<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeskList extends Model
{
    use HasFactory;

    protected static $unguarded = true;

    public function desk(): BelongsTo
    {
        return $this->belongsTo(Desk::class);
    }

    public function cards(): HasMany
    {
        return $this->hasMany(Card::class);
    }
}
