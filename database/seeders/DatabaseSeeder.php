<?php

namespace Database\Seeders;

use App\Models\Card;
use App\Models\Desk;
use App\Models\DeskList;
use App\Models\Task;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     => 'Mirsaid Akhmedov',
            'email'    => 'akhmedovmirik@gmail.com',
            'password' => '123123'
        ]);
        Desk::factory(5)->create();
        DeskList::factory(10)->create();
        Card::factory(20)->create();
        Task::factory(40)->create();
    }
}
